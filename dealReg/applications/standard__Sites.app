<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <tabs>standard-Chatter</tabs>
    <tabs>Branch__c</tabs>
    <tabs>Student__c</tabs>
    <tabs>MyCustomObject__c</tabs>
    <tabs>Dynamic_UI__c</tabs>
    <tabs>Dynamic_UI_Elements__c</tabs>
    <tabs>Template_Validation__c</tabs>
    <tabs>FormJourneyConfig__c</tabs>
    <tabs>BoatReview__c</tabs>
</CustomApplication>
